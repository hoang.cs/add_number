import 'package:flutter/material.dart';
import 'screen/main_screen.dart';

// final log = Logger('ADD CONTROLLER');
void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
          appBar: AppBar(
            title: const Center(child: Text('Add 2 numbers')),
          ),
          body: MainScreen()),
    );
  }
}
