import 'package:add_number/controller/add_num_controller.dart';
import 'package:flutter/material.dart';

class MainScreen extends StatelessWidget {
  MainScreen({Key? key}) : super(key: key);
  TextEditingController firstController = TextEditingController();
  TextEditingController secondController = TextEditingController();
  AddNumController addNumController = AddNumController();

  _onGetResultHandler(BuildContext ctx) {
    addNumController.setNum1 = firstController.text.split(' ').join();
    addNumController.setNum2 = secondController.text.split(' ').join();

    showDialog(
      context: ctx,
      builder: (_) => Center(
        child: Container(
          padding: const EdgeInsets.all(20),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Colors.white,
          ),
          height: 300,
          width: 300,
          child: addNumController.getMessageValidate().isEmpty
              ? Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    const Text(
                      'The result is',
                      style: TextStyle(fontSize: 30),
                    ),
                    Text(
                      addNumController.getResult(),
                      textAlign: TextAlign.center,
                    ),
                  ],
                )
              : Center(
                  child: Text(
                    addNumController.getMessageValidate(),
                    textAlign: TextAlign.center,
                    style: const TextStyle(fontSize: 30),
                  ),
                ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SizedBox(
        height: 300,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 20),
              child: TextField(
                controller: firstController,
                keyboardType: TextInputType.number,
                textAlign: TextAlign.center,
                decoration: InputDecoration(
                  hintText: 'Input first number',
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                    borderSide:
                        const BorderSide(color: Colors.blue, width: 2.0),
                  ),
                ),
              ),
            ),
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 20),
              child: TextField(
                controller: secondController,
                keyboardType: TextInputType.number,
                textAlign: TextAlign.center,
                decoration: InputDecoration(
                  hintText: 'Input second number',
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                    borderSide:
                        const BorderSide(color: Colors.blue, width: 2.0),
                  ),
                ),
              ),
            ),
            ElevatedButton(
              onPressed: () => _onGetResultHandler(context),
              child: const Text('Get result'),
              style: ElevatedButton.styleFrom(
                fixedSize: const Size(200, 50),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
