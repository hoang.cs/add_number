class StringUtils {
  static final numRegex = RegExp(r'(^[0-9]$)|(^[1-9][0-9]*$)');

  static bool isValidNumber(String num) {
    return numRegex.hasMatch(num);
  }
}
