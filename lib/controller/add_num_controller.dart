import 'dart:developer';

import 'package:add_number/utils/string_utils.dart';
import 'package:logging/logging.dart';

import '../main.dart';

class AddNumController {
  late String num1;
  late String num2;

  AddNumController({num1 = '0', num2 = '0'}) {
    this.num1 = num1.split('').reversed.join();
    this.num2 = num2.split('').reversed.join();
  }

  set setNum1(String num1) {
    this.num1 = num1.split('').reversed.join();
  }

  set setNum2(String num2) {
    this.num2 = num2.split('').reversed.join();
  }

  int _atomicAdd(String aNum1, String aNum2, int temp) {
    return int.parse(aNum1) + int.parse(aNum2) + temp;
  }

  String getMessageValidate() {
    if (num1.isEmpty || num2.isEmpty) {
      // log.shout('Empty input');
      log('[ERROR] Empty input');
      return 'Please initialize number(s)';
    }

    if (!StringUtils.isValidNumber(num1) || !StringUtils.isValidNumber(num2)) {
      log('[ERROR] Invalid numbers: $num1 $num2');
      return 'Invalid input';
    }

    return '';
  }

  String getResult() {
    int temp = 0;
    int maxLength = num1.length > num2.length ? num1.length : num2.length;
    String result = '';
    for (int i = 0; i < maxLength; i++) {
      String atomicNum1 = num1.length <= i ? '0' : num1[i];
      String atomicNum2 = num2.length <= i ? '0' : num2[i];

      int atomicAddResult = _atomicAdd(atomicNum1, atomicNum2, temp);

      temp = atomicAddResult ~/ 10;
      result += (atomicAddResult % 10).toString();

      log('[INFO] Added 2 atomic numbers: $atomicNum1 + $atomicNum2');
      log('[INFO] Atomic result: ${atomicAddResult % 10}');
      log('[INFO] Temp: $temp');
    }

    result += temp == 0 ? '' : temp.toString();
    return result.split('').reversed.join();
  }
}
